#
#    Mitchell Allen || Team Terrier
#    CpE 185 - 02   || Final Project
#    Wednesday 6:30 - 9:10pm
#    S. Kennedy
#    
#    Activates the environment and "hotword" function to ask
#    the Google Assistant questions

#!/bin/bash

source/home/pi/env/bin/activate
googlesamples-assistant-hotword --device-model-id wxyz