/*
  Mitchell Allen || Team Terrier
  CpE 185 - 02 || Final Project
  Wednesday 6:30 - 9:10
  S. Kennedy
*/
#include "simpletools.h"  //Include simple tools
#include "servo360.h"     //allows 360 servo control
#include "simpletext.h"   //allows serial communication 

#define C 1047
#define G 1568

int pinControl = 12;  //Control Wires (White, Red, Black) Connected to P12
int pinFeedback = 14; //Feedback wire (Yellow) Connected to P14 

void lock();    //Sets the servo to the locked possition
void unlock();  //Sets the servo to the unlocked position
void cntdwn();  //10 second countdown until the servo is set to the locked position

int main()                       
{
  servo360_connect(12, 14);                        //Connects the 360 Servo to the board by defining the pins || servo360_connect(pinControl, pinFeedback)
  while(1)
  {
    int s;
    print("Enter 0 for Lock, 1 for Unlock: ");    //If User inputs 0, door remains locked. 1 unlocks the door
    scan("%d", &s);
    print("\n");
    
    if(s == 0)
    {
      lock();
    }
    else if(s == 1)
    {
     unlock();
    }     
  }  
}

/////////////////////////////////////////
/*
  Sets the Servo to the locked position
  piezo buzzer beeps C note when finished
  flashes LEDS P26 + P27 when complete
*/
void lock()
{
  servo360_angle(12, -90);
  pause(1000);
  freqout(4, 500, C), high(26), high(27);
  pause(250), low(26), low(27);
  pause(250);
}
////////////////////////////////////
/*
  Sets servo position to unlocked
  wait 1 second
  initiates count down function
*/
void unlock()
{
  servo360_angle(12, 90);
  pause(1000);
  cntdwn();
}
////////////////////////////////////
/*
  10 second timer from unlocked postion
  Alternates flashing between LEDS P26 and P27 with beeping piezo buzzer (G note)
  Runs lock() function when finished with countdown
*/
void cntdwn()
{
  int i;
  for(i = 0; i < 10; i++)
  {
    if(i % 2 == 0)  //if the number is even, light up LED P26
    {
      freqout(4, 500, G), high(26);
      pause(250);
      low(26);
      pause(250);
    }
    else          //if the number is odd, light up LED P27
    {
      freqout(4, 500, G), high(27);
      pause(250);
      low(27);
      pause(250);
    }
  }        
  lock();
}  